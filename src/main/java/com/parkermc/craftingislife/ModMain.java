package com.parkermc.craftingislife;

import com.parkermc.craftingislife.proxy.ProxyCommon;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = ModMain.MODID, useMetadata = true)
public class ModMain{
    public static final String MODID = "craftingislife";
    
    @Instance(ModMain.MODID)
	public static ModMain instance;
    
    @SidedProxy(clientSide = "com.parkermc.craftingislife.proxy.ProxyClient", serverSide = "com.parkermc.craftingislife.proxy.ProxyServer", modId = MODID)
    public static ProxyCommon proxy;
    
    @EventHandler
    public void preInit(FMLPreInitializationEvent event){
    	proxy.preInit(event);
    }
    
    @EventHandler
    public void init(FMLInitializationEvent event) {
    	proxy.init(event);    
    }
    
    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
    	proxy.postInit(event);
    }

}

