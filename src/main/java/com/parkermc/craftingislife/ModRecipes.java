package com.parkermc.craftingislife;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class ModRecipes {
	
	public static void preInit() {
		ModTools.addShapedRecipe(new ResourceLocation(ModMain.MODID, "cobblestone_crafting_table"), null, new ItemStack(ModBlocks.cobblestone_crafting_table, 1), 
				"CC",
				"CC",
				'C', Blocks.COBBLESTONE);
		
		ModTools.addShapedRecipe(new ResourceLocation(ModMain.MODID, "cobblestone_furnace_1"), null, new ItemStack(ModBlocks.cobblestone_furnace, 1), 
				"CCC",
				"CcC",
				"CCC",
				'C', Blocks.COBBLESTONE,
				'c', Items.COAL);
		
		ModTools.addShapedRecipe(new ResourceLocation(ModMain.MODID, "cobblestone_furnace_2"), null, new ItemStack(ModBlocks.cobblestone_furnace, 1), 
				"CCC",
				"CcC",
				"CCC",
				'C', Blocks.COBBLESTONE,
				'c', new ItemStack(Items.COAL, 1, 1));
		
		// Dirt
		ModTools.addShapedRecipe(new ResourceLocation(ModMain.MODID, "dirt_crafting_table"), null, new ItemStack(ModBlocks.dirt_crafting_table, 1), 
				"DD",
				"DD",
				'D', Blocks.DIRT);
		
		ModTools.addShapedRecipe(new ResourceLocation(ModMain.MODID, "dirt_furnace_1"), null, new ItemStack(ModBlocks.dirt_furnace, 1), 
				"DDD",
				"DcD",
				"DDD",
				'D', Blocks.DIRT,
				'c', Items.COAL);
		
		ModTools.addShapedRecipe(new ResourceLocation(ModMain.MODID, "dirt_furnace_2"), null, new ItemStack(ModBlocks.dirt_furnace, 1), 
				"DDD",
				"DcD",
				"DDD",
				'D', Blocks.DIRT,
				'c', new ItemStack(Items.COAL, 1, 1));
		
		// Soul Sand
		ModTools.addShapedRecipe(new ResourceLocation(ModMain.MODID, "soul_sand_crafting_table"), null, new ItemStack(ModBlocks.soul_sand_crafting_table, 1), 
				"SS",
				"SS",
				'S', Blocks.SOUL_SAND);
		
		ModTools.addShapedRecipe(new ResourceLocation(ModMain.MODID, "soul_sand_furnace_1"), null, new ItemStack(ModBlocks.soul_sand_furnace, 1), 
				"SSS",
				"ScS",
				"SSS",
				'S', Blocks.SOUL_SAND,
				'c', Items.COAL);
		
		ModTools.addShapedRecipe(new ResourceLocation(ModMain.MODID, "soul_sand_furnace_2"), null, new ItemStack(ModBlocks.soul_sand_furnace, 1), 
				"SSS",
				"ScS",
				"SSS",
				'S', Blocks.SOUL_SAND,
				'c', new ItemStack(Items.COAL, 1, 1));
	}


}
