package com.parkermc.craftingislife;

import com.parkermc.craftingislife.blocks.*;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModBlocks {
	public static BlockWorkbench cobblestone_crafting_table;
	public static BlockFurnace cobblestone_furnace;
	public static BlockFurnace lit_cobblestone_furnace;
	
	public static BlockWorkbench dirt_crafting_table;
	public static BlockFurnace dirt_furnace;
	public static BlockFurnace lit_dirt_furnace;
	
	public static BlockWorkbench soul_sand_crafting_table;
	public static BlockFurnace soul_sand_furnace;
	public static BlockFurnace lit_soul_sand_furnace;
	
	public static void preInit() {
		// Cobblestone
		ModTools.registerBlock(cobblestone_crafting_table = new BlockWorkbench(Material.ROCK, "cobblestone", SoundType.STONE, 2.5f));
		ModTools.registerItem(new ItemBlock(cobblestone_crafting_table).setRegistryName(cobblestone_crafting_table.getRegistryName()));
		
		ModTools.registerBlock(cobblestone_furnace = new BlockFurnace(Material.ROCK, "cobblestone", SoundType.STONE, 3.5f, false));
		ModTools.registerItem(new ItemBlock(cobblestone_furnace).setRegistryName(cobblestone_furnace.getRegistryName()));
		
		ModTools.registerBlock(lit_cobblestone_furnace = new BlockFurnace(Material.ROCK, "cobblestone", SoundType.STONE, 3.5f, true));
		ModTools.registerItem(new ItemBlock(lit_cobblestone_furnace).setRegistryName(lit_cobblestone_furnace.getRegistryName()));
		
		BlockFurnace.addVariant(cobblestone_furnace, lit_cobblestone_furnace);
		
		// Dirt
		ModTools.registerBlock(dirt_crafting_table = new BlockWorkbench(Material.GROUND, "dirt", SoundType.GROUND, 1.5f));
		ModTools.registerItem(new ItemBlock(dirt_crafting_table).setRegistryName(dirt_crafting_table.getRegistryName()));
		
		ModTools.registerBlock(dirt_furnace = new BlockFurnace(Material.GROUND, "dirt", SoundType.GROUND, 2.5f, false));
		ModTools.registerItem(new ItemBlock(dirt_furnace).setRegistryName(dirt_furnace.getRegistryName()));
		
		ModTools.registerBlock(lit_dirt_furnace = new BlockFurnace(Material.GROUND, "dirt", SoundType.GROUND, 2.5f, true));
		ModTools.registerItem(new ItemBlock(lit_dirt_furnace).setRegistryName(lit_dirt_furnace.getRegistryName()));
		
		BlockFurnace.addVariant(dirt_furnace, lit_dirt_furnace);
		
		// Soul Sand
		ModTools.registerBlock(soul_sand_crafting_table = new BlockWorkbench(Material.SAND, "soul_sand", SoundType.SAND, 1.5f));
		ModTools.registerItem(new ItemBlock(soul_sand_crafting_table).setRegistryName(soul_sand_crafting_table.getRegistryName()));
		
		ModTools.registerBlock(soul_sand_furnace = new BlockFurnace(Material.SAND, "soul_sand", SoundType.SAND, 2.5f, false));
		ModTools.registerItem(new ItemBlock(soul_sand_furnace).setRegistryName(soul_sand_furnace.getRegistryName()));
		
		ModTools.registerBlock(lit_soul_sand_furnace = new BlockFurnace(Material.SAND, "soul_sand", SoundType.SAND, 2.5f, true));
		ModTools.registerItem(new ItemBlock(lit_soul_sand_furnace).setRegistryName(lit_soul_sand_furnace.getRegistryName()));
		
		BlockFurnace.addVariant(soul_sand_furnace, lit_soul_sand_furnace);
		
		
		GameRegistry.registerTileEntity(TileEntityFurnace.class, "other_furnace");
		
	}
	
	public static void initModels() {
		cobblestone_crafting_table.initModel();
		cobblestone_furnace.initModel();
		lit_cobblestone_furnace.initModel();
		
		dirt_crafting_table.initModel();
		dirt_furnace.initModel();
		lit_dirt_furnace.initModel();
		
		soul_sand_crafting_table.initModel();
		soul_sand_furnace.initModel();
		lit_soul_sand_furnace.initModel();
	}
}
