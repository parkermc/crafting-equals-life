package com.parkermc.craftingislife.proxy;

import java.io.File;

import com.parkermc.craftingislife.GuiHandler;
import com.parkermc.craftingislife.ModBlocks;
import com.parkermc.craftingislife.ModRecipes;

import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

public class ProxyCommon {
	public static File configFile;
	
	public void preInit(FMLPreInitializationEvent event) {
		ModBlocks.preInit();
		ModRecipes.preInit();
	}
	
	public void init(FMLInitializationEvent event) {
		GuiHandler.init();
	}
	
	public void postInit(FMLPostInitializationEvent event) {
	}
}
