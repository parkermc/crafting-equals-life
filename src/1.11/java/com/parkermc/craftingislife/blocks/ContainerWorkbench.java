package com.parkermc.craftingislife.blocks;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ContainerWorkbench extends net.minecraft.inventory.ContainerWorkbench{
	private BlockPos position;
	private World world;
	
	public ContainerWorkbench(InventoryPlayer playerInventory, World worldIn, BlockPos posIn){
		super(playerInventory, worldIn, posIn);
		this.position = posIn;
		this.world = worldIn;
	}
	
    @Override
    public boolean canInteractWith(EntityPlayer playerIn) {
    	return (!this.world.getBlockState(this.position).getBlock().getClass().equals(BlockWorkbench.class)) ? false : playerIn.getDistanceSq((double)this.position.getX() + 0.5D, (double)this.position.getY() + 0.5D, (double)this.position.getZ() + 0.5D) <= 64.0D;
    }
}
