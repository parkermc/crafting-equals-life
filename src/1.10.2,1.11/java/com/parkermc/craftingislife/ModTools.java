package com.parkermc.craftingislife;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModTools {

	public static final void addShapedRecipe(ResourceLocation name, ResourceLocation group, ItemStack output, Object... params) {
		GameRegistry.addShapedRecipe(output, params);
	}
	
	public static final void registerBlock(Block block) {
		GameRegistry.register(block);
	}
	
	public static final void registerItem(Item item) {
		GameRegistry.register(item);
	}
}
